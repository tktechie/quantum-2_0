---
layout: project
project: Seurat 
image: project_seurat.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Indianapolis, Indiana

**Square Footage**
<br /> 37,110 square feet

**Services Provided**
<br /> Programming, Space Planning, Interior Design, Construction Documents

**The Challenge**
<br /> To create a hip, yet sophisticated environment on a minimal budget of $19.99 per square foot.

**The Result**
<br /> With this investment, Seurat received an upbeat, progressive image that supported their culture, marketing efforts, and improved morale.