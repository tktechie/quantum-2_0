---
layout: project
project: International Outsourcing Services
image: project_ios.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Bloomington, Indiana

**Square Footage**
<br /> 11,267 square feet

**Services Provided**
<br /> Interior Design, Furniture Selection and Procurement

**The Challenge**
<br /> To develop a design image that projected a worldclass headquarters for their company.

**The Result**
<br /> Through integration of technology and traditional design, a timeless and world-class facility was developed around the I.O.S. company.