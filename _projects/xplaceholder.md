---
layout: project
project: ProjectX
image: placeholder.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Secret, Indiana

**Square Footage**
<br /> X,XXX square feet

**Services Provided**
<br /> Yep. Services were provided.

**The Challenge**
<br /> We aren't supposed to have an ego. But just sayin', we're awesome..

**The Result**
<br /> You'd have to see for yourself. It's indescribable.


