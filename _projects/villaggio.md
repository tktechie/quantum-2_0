---
layout: project
project: Villaggio Day Spa
image: project_villaggio.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Fishers, Indiana

**Square Footage**
<br /> 4,781 square feet

**Services Provided**
<br /> Space Planning, Interior Design, Construction Documents, Art Direction, Accessory and Furniture Selection

**The Challenge**
<br /> To embody a crisp, yet sophisticated atmosphere that appeals to both the male and female demographic.

**The Result**
<br /> A space was created having a traditional base with a modern edge that flawlessly combines feminine motifs with a more masculine color palette using espresso brown and lavender. Soon after the spa opened, a second location was planned on the northwest side of the city due to its demand and success.

**Villaggio Day Spa was featured in an article in Interior Design Magazine**
