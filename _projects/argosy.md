---
layout: project
project: Argosy Hotel and Spa
image: project_argosy.jpg
imageAlt:
dateComplete: 
---


**Location**
<br /> Riverside, Missouri

**Square Footage**
<br /> 220,000 square foot, 260 room hotel

**Project Cost**
<br /> $50 Million

**Services Provided**
<br /> Concept and Theme Development, Art Direction, Interior Architecture and Design, Construction Documents, Furniture Selection, Prop Procurement, Site Supervision

**The Challenge**
<br /> To design a hotel and spa interior that would feel luxurious and contemporary to the guests, while continuing the "old world" concept from the rest of the adjoining casino.

**The Result**
<br /> A luxurious, boutique style hotel was created with a more modern take on "old world" elegance.

**Argosy Hotel and Spa was awarded by the International Interior Design Association's Indiana chapter for best "Hospitality" project and "Best of Show!"**
  