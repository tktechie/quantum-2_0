---
layout: project # < Don't ever change this!!
# These are necessary.
project: ProjectX # < Title of the project
image: placeholder.jpg
# These are optional. The info will be used if provided.
imageAlt:
dateComplete: 2018-01-01 # < Must use this date format YYYY-MM-DD. 
---

{% comment %} You don't have to use the following sections neccessarily, they were from the original website. This is Markdown format. See the instructions in the main site folder for mor info. {% endcomment %}

**Location**
<br /> Ynknown, Indiana

**Square Footage**
<br /> X,XXX square feet

**Services Provided**
<br /> Yep. Services were provided.

**The Challenge**
<br /> We aren't supposed to have an ego. But just sayin', we're awesome..

**The Result**
<br /> You'd have to see for yourself. It's indescribable.


