---
layout: project
project: Carmel Arts Building Model Condo
image: project_carmelarts.jpg
imageAlt:
dateComplete: 
---

**Location**
<br />Carmel, Indiana

**Square Footage**
<br />3,511 square feet

**Services Provided**
<br />Accessory and Furniture Selection, Procurement, and Installation

**The Challenge**
<br />To create the "urban loft" style that would appeal to the residential clientele of downtown Carmel, Indiana.

**The Result**
<br />The model condo features a mix of furniture with strong, architectural lines mixed with softer accessory elements. A unique balance is created by mixing both contemporary and traditional pieces.