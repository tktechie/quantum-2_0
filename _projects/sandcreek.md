---
layout: project
project: Sand Creek Country Club
image: project_sandcreek.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Chesterton, Indiana

**Square Footage**
<br /> 45,000 square feet - includes two restaurants, bar, lounges, locker facilities, exercise center, pro shop, and support areas

**Services Provided**
<br /> Interior Design and Furniture Selection

**The Challenge**
<br /> Establishing a classic traditional image for the Club members that was both comfortable and elegant.

**The Result**
<br /> The overall interior design appealed to a wide age demographic as well as the traditional feel was both comfortable and timeless. Natural materials of wood, stone, iron and glass create a very inviting and relaxing ambience.