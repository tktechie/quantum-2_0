---
layout: project
project: Interactive Intelligence
image: project_interactive_2.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Indianapolis, Indiana

**Square Footage**
<br /> 120,000 square feet

**Services Provided**
<br /> Programming, Space Planning, Interior Design, Construction Documents, Furniture Consultation

**The Challenge**
<br /> After successfully designing Interactive Intelligence's first headquarters, we were invited back to design their new International Headquarters. We were challenged to create a flexible interactive environment while still maintaining their corporate standard of hard walled offices.

**The Result**
<br /> We maintained Interactive's fun & progressive environment while taking their image to a more sophisticated, world-class level. Designated gathering spaces assist in communication and interaction between departments as well as promote "out of the box" thinking.

*"The first impression of our lobby has influenced the perception of our customers."*
<br /> **-Ty Baldwin, VP of sales**