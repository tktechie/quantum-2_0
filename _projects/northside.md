---
layout: project
project: New Northside Hotel (Brandt waiting approval)
image: project_northside.jpg
imageAlt:
dateComplete: 
---

**Location**
<br /> Indianapolis, Indiana

**Square Footage**
<br /> 135,000 square feet

**Services Provided**
<br /> Masterplanning, Architecture & Interior Design

**The Challenge**
<br /> To expand the prototype model for a premiere 7-story hotel to include: an upscale restaurant, a Business Executive conference center, and a spa.

**The Result**
<br /> This project is in the financing process.