---
layout: project
project: Interactive Intelligence
image: project_interactive.jpg
imageAlt:
dateComplete: 
---

**Square Footage**
<br /> 35,000 square feet

**Services Provided**
<br /> Programming, Space Planning, Interior Design, Furniture

**The Challenge**
<br /> To develop a functional and effective design that would represent Interactive's world class product and their innovative corporate culture.

**The Result**
<br /> The creative and progressive environment supported their culture while taking their image to a more sophisticated, world class level; pushing the envelope with the design the same way they push the envelope with their product.