---
layout: project
project: "Claddagh Irish Pub & Restaurant"
image: project_claddagh.jpg
imageAlt:
dateComplete: 
---

**Location**
<br />Nationwide

**Square Footage**
<br />6,000-8,000 square feet

**Services Provided**
<br />Interior Design, Architecture

**The Challenge**
<br />To evaluate space efficiencies with the goal of increasing head count per square foot; create and implement design standards for new facilities nationwide while giving each space historically themed design characteristics.

**The Result**
<br />Increased efficiencies in the dining areas incorporated more seating capacity for a greater return on investment. New design standards were developed reducing maintenance costs and increasing life cycle performance. Updated, yet historically correct designs were implemented attracting more customers.